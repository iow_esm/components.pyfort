#!/bin/bash

debug=${1:-"release"}
rebuild=${2:-"fast"}

module unload intel
module unload impi
module load intel/2021.2
module load impi/2021.2

module load anaconda3/2019.10

FC=mpiifort
AR="ar"

IOW_ESM_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/../.."

if [ $debug == "debug" ]; then
	FFLAGS="-O0 -r8 -fp-model precise -xHost -DUSE_DOUBLE_PRECISION -g -traceback -check all -DIOW_ESM_DEBUG"
	configuration="DEBUG"
else
	FFLAGS="-O3 -r8 -no-prec-div -fp-model fast=2 -xHost -DUSE_DOUBLE_PRECISION"
	configuration="PRODUCTION"
fi

source ./compile.sh


