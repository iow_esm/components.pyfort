build_dir="build_${configuration}"
bin_dir="bin_${configuration}"

if [ "$rebuild" == "rebuild" ]; then
	rm -r "${build_dir}"
fi
mkdir -p ./"${build_dir}"

cd "${build_dir}"

python3 ../src/pyfort_src.py
$FC -c -Wl,-rpath,"${PWD}" -L"${PWD}" -lpyfort ../src/call_python.f90

library="libcall_python.a" 

if [ -f ${library} ]; then
    rm ${library}
fi
$AR rcv ${library} call_python.o

#$AR cqT combined.a ${library} $LIBS  && echo -e 'create combined.a\naddlib combined.a\nsave\nend' | $AR -M

#mv combined.a ${library}

cd -

#cd src
